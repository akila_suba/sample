FROM tomcat:7.4.63-jdk8-openjdk-slim-buster

ENV CATALINA_OUT="/dev/stdout"

WORKDIR /

COPY target/*.war /usr/local/tomcat/webapps/ROOT.war

ENTRYPOINT ["/usr/local/tomcat/bin/catalina.sh", "run"]
